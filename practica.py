import pymongo
from bson.objectid import ObjectId

MONGO_HOST="localhost"
MONGO_PUERTO="27017"
MONGO_TIEMPO_FUERA=1000

MONGO_URI="mongodb://"+MONGO_HOST+":"+MONGO_PUERTO+"/"

MONGO_BASEDATOS="diccionario"
MONGO_COLECCION="diccionary"
cliente=pymongo.MongoClient(MONGO_URI,serverSelectionTimeoutMS=MONGO_TIEMPO_FUERA)
baseDatos=cliente[MONGO_BASEDATOS]
coleccion=baseDatos[MONGO_COLECCION]

def agregarPalabra():
    nuevapalabra = input("Ingrese la nueva palabra: ")
    nuevoSignificado= input("Ingrese su significado: ")
    documento={"palabra":nuevapalabra, "significado": nuevoSignificado}
    coleccion.insert(documento)
    print("palabra agregada!")

def editarPalabra():
    for documento in coleccion.find():
        print(documento["palabra"]+": "+documento["significado"])
    palabraaeditar = input("Ingrese la palabra a editar: ")
    cambiar_palabra ={"palabra" : palabraaeditar}
    coleccion.delete_one(cambiar_palabra)
    palabra_new = input("Ingrese la nueva palabra: ")
    new_significado= input("Ingrese el significado: ")
    documentoo= {"palabra":palabra_new, "significado":new_significado}
    coleccion.insert(documentoo)
    print("palabra editada!")

def mostrarPalabras():
    try:
        for documento in coleccion.find():
            print(documento["palabra"])
        
        cliente.close()
    except pymongo.errors.ServerSelectionTimeoutError as errorTiempo:
        print("Tiempo excedido"+ errorTiempo)

    except pymongo.errors.ConnectionFailure as errorConexion:
        print("Fallo al conectarse a mongo"+ errorConexion)

def eliminarPalabra():
    for documento in coleccion.find():
            print(documento["palabra"])
    borrar_palabra= input("Ingrese la palabra que desea eliminar: ")
    palabraaborrar={"palabra" : borrar_palabra}
    coleccion.delete_one(palabraaborrar)
    print("palabra eliminada!")
  

def buscarsignificado():
    try:
        for documento in coleccion.find():
            print(documento["palabra"]+": "+documento["significado"])
            cliente.close()
    except pymongo.errors.ServerSelectionTimeoutError as errorTiempo:
        print("Tiempo excedido"+ errorTiempo)


repetir_bucle = True
while repetir_bucle:
    print("Diccionario de Panamá")
    print("1.agregar palabra")
    print("2.Editar palabra existente")
    print("3.Mostrar palabras")
    print("4.Eliminar palabra")
    print("5.Buscar significado")
    print("6.Salir")
    opc= int(input("Que desea hacer?: "))
    if opc== 1:
        agregarPalabra() 
    elif opc==2:
        editarPalabra()
    elif opc==3: 
        mostrarPalabras()
    elif opc== 4:
        eliminarPalabra()
    elif opc == 5:
        buscarsignificado()
    elif opc ==6:
        repetir_bucle= False
    else:
        print("Opcion incorrecta")

print("Gracias por usar nuestros servicios")
